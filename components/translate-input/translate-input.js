const TranslateInput = Ractive.extend({
  data: { isGlobal: false },
  template: `
    <div class="mui-textfield">
      <input type="text" value='{{translations[language]}}' />
      <label>{{language}}</label>
    </div>
  `
});

export default TranslateInput;