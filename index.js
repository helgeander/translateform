function getResult(result) {
  return Array.isArray(result) ? result[0][0][0] : '';
}

var ractive = new Ractive({
	target: '#main',
  template: '#template',
  components: {
    TranslateInput: import('./components/translate-input/translate-input.js')
  },
	data: {
    translations: {
      no: "",
      en: "",
      de: "",
      es: "",
      sv: ""
    }
  },

  async translateText (sourceLang, targetLang, sourceText) {
    let url = `https://translate.googleapis.com/translate_a/single?client=gtx&sl=${sourceLang}&tl=${targetLang}&dt=t&q=${encodeURI(sourceText)}`;

    const response = await fetch(url);
    const result = await response.json();
    return this.set(`translations.${targetLang}`, getResult(result));
  },

  translate () {
    let translations = this.get('translations');
    let languages = Object.keys(translations);
    let sourceLang = languages.find(l => translations[l].length > 0);

    if (!sourceLang) return;

    let sourceText = translations[sourceLang];

    languages
      .filter(l => l != sourceLang)
      .forEach(l => this.translateText(sourceLang, l, sourceText));
  }
});